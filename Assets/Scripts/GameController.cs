﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    #region Fields
    private static GameController _instance;
    private GameState _currentState;
    private GameState _oldState;
    private bool isGamePaused = false;
    private MainMenuStateView _view;
    private Camera _cam;
    private AudioManager _audioManger;
    private AudioSource _backgroundMusicSource;
    private AudioSource _monsterDeathSource;
    private AudioSource _monsterRoamingSource;
    private AudioSource _gunFireSource;
    private GameObject _enemy;
    private GameObject[] _spawnPoints;
    private float _spawnFrequancy = 1;
    private int _totalMonster = 0;

    private PlayerView _playerView;
    private PlayerController _playerController;
    private Player _playerModel;
    public AudioSource GunFireSource
    {
        get { return _gunFireSource; }
    }

    public AudioSource MonsterDeathSource
    {
        get { return _monsterDeathSource; }
    }
    public AudioManager AudioManager
    {
        get { return _audioManger; }
    }
    public GameState OldState
    {
        get { return _oldState; }
        set { _oldState = value; }
    }

    public GameState CurrentState
    {
        get { return _currentState; }
        set { _currentState = value; }
    }
    public static GameController Instance
    {
        get { return _instance; }
    }

    public int TotalMonster
    {
        get { return _totalMonster; }
        set { _totalMonster = value; }
    }
    #endregion 
    // Start is called before the first frame update
    void Start()
    {
        _instance = this;
        _spawnPoints=GameObject.FindGameObjectsWithTag("SpawnPoint");
        _enemy = Resources.Load<GameObject>("GamePlay/Zombie1");
        _audioManger = new AudioManager();
        _backgroundMusicSource = this.gameObject.AddComponent<AudioSource>();
        _monsterDeathSource = this.gameObject.AddComponent<AudioSource>();
        _monsterRoamingSource = this.gameObject.AddComponent<AudioSource>();
        _gunFireSource = this.gameObject.AddComponent<AudioSource>();
        //Instantiate MainMenuState
        if (SceneManager.GetActiveScene().buildIndex==0)
        {
            CreateMainMenuState();
        }
        else
        {
            CreatePlayerAndCameraMVC();
            GameObject gameObject = new GameObject("CoroutineManager");
            gameObject.AddComponent<CoroutineManager>();
            StartCoroutine(SpawnEnemies(_spawnFrequancy));
        }

    
    }

    private void CreateMainMenuState()
    {
        _view = MainMenuFactory.Instance.CreateMainMenuStateView(this.transform);
        MainMenuStateModel model = MainMenuFactory.Instance.CreateMainMenuStateModel();
        MainMenuState state = MainMenuFactory.Instance.CreateMainMenuState(_view, model);
        _currentState = null;

        if(SceneManager.GetActiveScene().buildIndex!=0)
        {
            _view.BackgroundImage.enabled = false;
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            isGamePaused = !isGamePaused;

            if(isGamePaused)
            {
                Cursor.visible = true;
                Time.timeScale = 0;
                CreateMainMenuState();
            }
            else
            {
                Cursor.visible = false;
                Destroy(_view.gameObject);
                Time.timeScale = 1;
            }
        }
    }

    private void CreatePlayerAndCameraMVC()
    {
        _playerView = PlayerFactory.Instance.CreatePlayerView(new Vector3(0,1,0));
        _playerModel = PlayerFactory.Instance.CreatePlayer();
        _playerController = PlayerFactory.Instance.CreatePlayerController(_playerView, _playerModel); ;

        MainCameraView mainCameraView = Camera.main.gameObject.GetComponent<MainCameraView>();
        MainCamera mainCameraModel = MainCameraFactory.Instance.CreateMainCamera();
        MainCameraController mainCameraController = MainCameraFactory.Instance.CreateMainCameraController(mainCameraView, mainCameraModel);
        mainCameraView.Player = _playerView.transform;
        Cursor.visible = false;
    }

    private IEnumerator SpawnEnemies(float frequancy)
    {
        yield return new WaitForSeconds(frequancy);
        if (_totalMonster<15)
        {
            EnemyView view = EnemyFactory.Instance.CreateEnemyView(_spawnPoints[Random.Range(0, _spawnPoints.Length)]);
            view.Player = _playerView.gameObject;
            Enemy model = EnemyFactory.Instance.CreateEnemy();
            EnemyController controller = EnemyFactory.Instance.CreateEnemyController(view, model);
            _totalMonster++;
        }

        CoroutineManager.Instance.StartCoroutine((SpawnEnemies(_spawnFrequancy)));
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}