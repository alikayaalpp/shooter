﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFactory : Singleton<EnemyFactory>
{
    #region Fields
    //private static EnemyFactory _instance = new EnemyFactory();

    //public static EnemyFactory Instance
    //{
    //    get { return _instance; }
    //}
    #endregion

    public EnemyView CreateEnemyView(GameObject obj)
    {
        GameObject viewObject = Resources.Load<GameObject>("GamePlay/Zombie1");
        viewObject = GameObject.Instantiate(viewObject, obj.transform.position, Quaternion.identity);
        EnemyView view = viewObject.GetComponent<EnemyView>();
        return view;
    }

    public Enemy CreateEnemy()
    {
        Enemy model = new Enemy();
        return model;
    }

    public EnemyController CreateEnemyController(EnemyView view, Enemy model)
    {
        EnemyController controller = new EnemyController(view, model);
        return controller;
    }

    public EnemyHealth CreateEnemyHealth()
    {
        EnemyHealth model = new EnemyHealth();
        return model;
    }

    public EnemyHealthController CreateEnemyHealthController(EnemyHealthView view, EnemyHealth model)
    {
        EnemyHealthController controller = new EnemyHealthController(view, model);
        return controller;
    }

    public EnemyMovement CreateEnemyMovement()
    {
        EnemyMovement model = new EnemyMovement();
        return model;
    }

    public EnemyMovementController CreateEnemyMovementController(EnemyMovementView view, EnemyMovement model)
    {
        EnemyMovementController controller = new EnemyMovementController(view, model);
        return controller;
    }
}
