﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFactory:Singleton<BulletFactory>
{
    //#region Fields
    //private static BulletFactory _instance = new BulletFactory();

    //public static BulletFactory Instance
    //{
    //    get { return _instance; }
    //}
    //#endregion

    public MachineGunBulletView CreateMachineGunBulletView(Transform parent)
    {
        GameObject viewObject = Resources.Load<GameObject>("GamePlay/MachineGunBullet");
        viewObject = GameObject.Instantiate(viewObject, parent);
        MachineGunBulletView view = viewObject.GetComponent<MachineGunBulletView>();
        return view;
    }

    public MachineGunBullet CreateMachineGunBullet()
    {
        MachineGunBullet model = new MachineGunBullet();
        return model;
    }

    public MachineGunBulletController CreateMachinbeGunBulletController(MachineGunBulletView view, MachineGunBullet model)
    {
        MachineGunBulletController controller = new MachineGunBulletController(view, model);
        return controller;
    }
}
