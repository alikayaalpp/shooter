﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunFactory:Singleton<GunFactory>
{
    #region Fields
    //private static GunFactory _instance = new GunFactory();

    //public static GunFactory Instance
    //{
    //    get { return _instance; }
    //}
    #endregion

    public MachineGunView CreateMachineGunView(Transform parent)
    {
        GameObject viewObject = Resources.Load<GameObject>("GamePlay/MachineGun");
        viewObject = GameObject.Instantiate(viewObject, parent);
        MachineGunView view = viewObject.GetComponent<MachineGunView>();
        return view;
    }

    public MachineGun CreateMachineGun()
    {
        MachineGun model = new MachineGun();
        return model;
    }

    public MachineGunController CreateMachineGunController(MachineGunView view, MachineGun model)
    {
        MachineGunController controller = new MachineGunController(view, model);
        return controller;
    }
}
