﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MadeByFactory : Singleton<MadeByFactory>
{
    #region Fields
    //private static MadeByFactory _instance = new MadeByFactory();

    //public static MadeByFactory Instance
    //{
    //    get { return _instance; }
    //}
    #endregion

    public MadeByStateView CreateMadeByStateView(Transform parent)
    {
        GameObject viewObject = Resources.Load<GameObject>("UI/MadeByPanel");
        viewObject = GameObject.Instantiate(viewObject, parent);
        MadeByStateView view = viewObject.GetComponent<MadeByStateView>();
        return view;
    }

    public MadeByStateModel CreateMadeByStateModel()
    {
        MadeByStateModel model = new MadeByStateModel();
        return model;
    }

    public MadeByState CreateMadeByState(MadeByStateView view, MadeByStateModel model)
    {
        MadeByState state = new MadeByState(view, model);
        return state;
    }
}
