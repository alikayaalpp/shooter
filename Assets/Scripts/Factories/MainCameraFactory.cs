﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraFactory:Singleton<MainCameraFactory>
{
    #region Fields
    //private static MainCameraFactory _instance = new MainCameraFactory();

    //public static MainCameraFactory Instance
    //{
    //    get { return _instance; }
    //}
    #endregion

    public MainCameraView CreateMainCameraView()
    {
        GameObject viewObject = Resources.Load<GameObject>("GamePlay/Main Camera");
        viewObject = GameObject.Instantiate(viewObject);
        MainCameraView view = viewObject.GetComponent<MainCameraView>();
        return view;
    }

    public MainCamera CreateMainCamera()
    {
        MainCamera model = new MainCamera();
        return model;
    }

    public MainCameraController CreateMainCameraController(MainCameraView view, MainCamera model)
    {
        MainCameraController controller = new MainCameraController(view, model);
        return controller;
    }
}
