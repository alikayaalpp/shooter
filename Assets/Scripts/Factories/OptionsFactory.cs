﻿using UnityEngine;

public class OptionsFactory:Singleton<OptionsFactory>
{
    #region Fields
    //private static OptionsFactory _instance = new OptionsFactory();

    //public static OptionsFactory Instance
    //{
    //    get { return _instance; }
    //}
    #endregion

    public OptionsMenuStateView CreateOptionsMenuView(Transform parent)
    {
        GameObject viewObject = Resources.Load<GameObject>("UI/OptionsPanel");
        viewObject = GameObject.Instantiate(viewObject, parent);
        OptionsMenuStateView view = viewObject.GetComponent<OptionsMenuStateView>();
        return view;
    }

    public OptionsMenuStateModel CreateOptionsMenuStateModel()
    {
        OptionsMenuStateModel model = new OptionsMenuStateModel();
        return model;
    }

    public OptionsMenuState CreateOptionsMenuState(OptionsMenuStateView view, OptionsMenuStateModel model)
    {
        OptionsMenuState state = new OptionsMenuState(view, model);
        return state;
    }

    public AudioView CreateAudioView(Transform parent)
    {
        GameObject viewObject = Resources.Load<GameObject>("UI/AudioPanel");
        viewObject = GameObject.Instantiate(viewObject, parent);
        AudioView view = viewObject.GetComponent<AudioView>();
        return view;
    }

    public Audio CreateAudio()
    {
        Audio model = new Audio();
        return model;
    }

    public AudioController CreateAudioController(AudioView view, Audio model)
    {
        AudioController controller = new AudioController(view,model);
        return controller;
    }

    public HowToPlayView CreateHowToPlayView(Transform parent)
    {
        GameObject viewObject = Resources.Load<GameObject>("UI/HowToPlayPanel");
        viewObject = GameObject.Instantiate(viewObject, parent);
        HowToPlayView view = viewObject.GetComponent<HowToPlayView>();
        return view;
    }

    public HowToPlay CreateHowToPlay()
    {
        HowToPlay model = new HowToPlay();
        return model;
    }

    public HowToPlayController CreateHowToPlayController(HowToPlayView view, HowToPlay model)
    {
        HowToPlayController controller = new HowToPlayController(view,model);
        return controller;
    }

    public VideoView CreateVideoView(Transform parent)
    {
        GameObject viewObject = Resources.Load<GameObject>("UI/VideoPanel");
        viewObject = GameObject.Instantiate(viewObject, parent);
        VideoView view = viewObject.GetComponent<VideoView>();
        return view;
    }

    public Video CreateVideo()
    {
        Video model = new Video();
        return model;
    }

    public VideoController CreateVideoController(VideoView view, Video model)
    {
        VideoController controller = new VideoController(view, model);
        return controller;
    }
}
