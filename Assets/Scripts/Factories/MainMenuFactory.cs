﻿using UnityEngine;

public class MainMenuFactory:Singleton<MainMenuFactory>
{
    #region Fields
    //private static MainMenuFactory _instance = new MainMenuFactory();
    //public static MainMenuFactory Instance
    //{
    //    get { return _instance; }
    //}
    #endregion
   
    public MainMenuStateView CreateMainMenuStateView(Transform parent)
    {
        GameObject viewObject = Resources.Load<GameObject>("UI/MainMenu");
        viewObject = GameObject.Instantiate(viewObject, parent);
        MainMenuStateView view = viewObject.GetComponent<MainMenuStateView>();
        return view;
    }

    public MainMenuStateModel CreateMainMenuStateModel()
    {
        MainMenuStateModel mainMenuStateModel = new MainMenuStateModel();
        return mainMenuStateModel;
    }

    public MainMenuState CreateMainMenuState(MainMenuStateView view, MainMenuStateModel model)
    {
        MainMenuState mainMenuState = new MainMenuState(view, model);
        return mainMenuState;
    }
}
