﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFactory:Singleton<PlayerFactory>
{
    #region Fields
    //private static PlayerFactory _instance = new PlayerFactory();
    //#endregion
    //public static PlayerFactory Instance
    //{
    //    get { return _instance; }
    //}
    #endregion

    public PlayerView CreatePlayerView(Vector3 pos)
    {
        GameObject viewObject = Resources.Load<GameObject>("GamePlay/Player");
        viewObject = GameObject.Instantiate(viewObject);
        viewObject.transform.position = pos;
        PlayerView view = viewObject.GetComponent<PlayerView>();
        return view;
    }

    public Player CreatePlayer()
    {
        Player model = new Player();
        return model;
    }

    public PlayerController CreatePlayerController(PlayerView view, Player model)
    {
        PlayerController controller = new PlayerController(view, model);
        return controller;
    }

    public PlayerHealth CreatePlayerHealth()
    {
        PlayerHealth model = new PlayerHealth();
        return model;
    }

    public PlayerHealthController CreatePlayerHealthController(PlayerHealthView view, PlayerHealth model)
    {
        PlayerHealthController controller = new PlayerHealthController(view, model);
        return controller;
    }

    public PlayerMovement CreatePlayerMovement()
    {
        PlayerMovement model = new PlayerMovement();
        return model;
    }

    public PlayerMovementController CreatePlayerMovementController(PlayerMovementView view, PlayerMovement model)
    {
        PlayerMovementController controller = new PlayerMovementController(view, model);
        return controller;
    }
}