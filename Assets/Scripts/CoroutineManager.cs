﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineManager : MonoBehaviour
{
    private static CoroutineManager _instance = new CoroutineManager();

    public static CoroutineManager Instance
    {
        get { return _instance; }
    }

    private void Start()
    {
        _instance = this;
    }
}
