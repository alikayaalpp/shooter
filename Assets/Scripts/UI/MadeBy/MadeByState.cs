﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MadeByState : GameState
{
    #region Fields
    private MadeByStateModel _model;
    private MadeByStateView _view;
    #endregion

    public MadeByState(MadeByStateView view, MadeByStateModel model)
    {
        _model = model;
        _view = view;
        Begin();
    }

    public override void Begin()
    {
        base.Begin();
        InitializeInnerMVCs();
    }

    public override void End()
    {
        base.End();
        GameObject.Destroy(_view.gameObject);
    }

    private void InitializeInnerMVCs()
    {

    }

}
