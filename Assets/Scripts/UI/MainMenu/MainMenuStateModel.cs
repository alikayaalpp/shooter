﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuStateModel
{
    #region EventHandlers
    public event EventHandler<SwitchStateEventArgs> SwitchStateEvent;
    #endregion
    public void OnOptionButtonPressed(object sender, OptionButtonPressedEventArgs eventArgs)
    {
        if(GameController.Instance.CurrentState is  OptionsMenuState)
        {
            return;
        }

        GameController.Instance.OldState = GameController.Instance.CurrentState;
        OptionsMenuStateView view = OptionsFactory.Instance.CreateOptionsMenuView(eventArgs.Parent);
        OptionsMenuStateModel model = OptionsFactory.Instance.CreateOptionsMenuStateModel();
        OptionsMenuState state = OptionsFactory.Instance.CreateOptionsMenuState(view, model);
        GameController.Instance.CurrentState = state;
        SwitchStateEvent?.Invoke(this , new SwitchStateEventArgs());
    }

    public void OnMadeByButtonPressed(object sender, MadeByButtonPresseEventArgs eventArgs)
    {
        if(GameController.Instance.CurrentState is MadeByState)
        {
            return;
        }

        GameController.Instance.OldState = GameController.Instance.CurrentState;
        MadeByStateView view = MadeByFactory.Instance.CreateMadeByStateView(eventArgs.Parent);
        MadeByStateModel model = MadeByFactory.Instance.CreateMadeByStateModel();
        MadeByState state = MadeByFactory.Instance.CreateMadeByState(view, model);
        GameController.Instance.CurrentState = state;
        SwitchStateEvent?.Invoke(this, new SwitchStateEventArgs());
    }

    public void OnBackButtonPressed(object sender, BackbButtonPressedEventArgs eventArgs)
    {
        GameState tempState = GameController.Instance.CurrentState;
        GameController.Instance.CurrentState = GameController.Instance.OldState;
        GameController.Instance.OldState = tempState;
        SwitchStateEvent?.Invoke(this, new SwitchStateEventArgs());
    }

    public void OnExitGameButtonPressed(object sender, ExitGameButtonEventArgs eventArgs)
    {
        GameController.Instance.ExitGame();
    }
}
