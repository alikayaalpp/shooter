﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuState : GameState
{
    #region Fields
    private MainMenuStateView _view;
	private MainMenuStateModel _model;
    #endregion

    public MainMenuState(MainMenuStateView view, MainMenuStateModel model)
	{
        _view = view;
        _model = model;
        Begin();
    }

    public override void Begin()
    {
        base.Begin();
        RegisterEvents();
    }

    public override void End()
    {
        base.End();
        UnregisterEvents();
        GameObject.Destroy(_view);
    }
    private void RegisterEvents()
    {
        _view.OptionButtonPressedEvent += _model.OnOptionButtonPressed;
        _view.ExitGameButtonEvent += _model.OnExitGameButtonPressed;
        _view.MadeByButtonPresseEvent += _model.OnMadeByButtonPressed;
        _view.BackbButtonPressedEvent += _model.OnBackButtonPressed;
        _model.SwitchStateEvent += OnStateSwitched;
    }

    private void UnregisterEvents()
    {
        _view.OptionButtonPressedEvent -= _model.OnOptionButtonPressed;
        _view.ExitGameButtonEvent -= _model.OnExitGameButtonPressed;
        _view.MadeByButtonPresseEvent -= _model.OnMadeByButtonPressed;
        _view.BackbButtonPressedEvent -= _model.OnBackButtonPressed;
        _model.SwitchStateEvent -= OnStateSwitched;
    }

    private void OnStateSwitched(object sender, SwitchStateEventArgs eventArgs)
    {
        GameState oldState = GameController.Instance.OldState;
        GameState state = GameController.Instance.CurrentState;
        
        if(oldState is OptionsMenuState)
        {
            (oldState as OptionsMenuState).End();
        }

        if(oldState is MadeByState)
        {
            (oldState as MadeByState).End();
        }

        if(state is OptionsMenuState)
        {
            (state as OptionsMenuState).Begin();
        }

        if(state is MadeByState)
        {
            (state as MadeByState).Begin();
        }

    }
}
