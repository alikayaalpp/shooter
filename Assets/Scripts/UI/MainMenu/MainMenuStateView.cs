﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuStateView : MonoBehaviour
{
    #region Fields
    [SerializeField] private Button _startGameButton;
    [SerializeField] private Button _optionsButton;
    [SerializeField] private Button _madeByButton;
    [SerializeField] private Button _exitGameButton;
    [SerializeField] private Button _backButton;
    [SerializeField] private Transform _mainMenuContentPanel;
    [SerializeField] private Image _backgroundImage;

    public Image BackgroundImage
    {
        get { return _backgroundImage; }
        set { _backgroundImage = value; }
    }
    #endregion

    #region Actions
    public event EventHandler<OptionButtonPressedEventArgs> OptionButtonPressedEvent;
    public event EventHandler<ExitGameButtonEventArgs> ExitGameButtonEvent;
    public event EventHandler<MadeByButtonPresseEventArgs> MadeByButtonPresseEvent;
    public event EventHandler<BackbButtonPressedEventArgs> BackbButtonPressedEvent;

    #endregion
    void Start()
    {
        _startGameButton.onClick.AddListener(OnStartGameButtonPressed);
        _optionsButton.onClick.AddListener(OnOptionsButtonPressed);
        _madeByButton.onClick.AddListener(OnMadeByButtonPressed);
        _exitGameButton.onClick.AddListener(OnExitGameButtonPressed);
        _backButton.onClick.AddListener(OnBackButtonPressed);
    }

    private void OnStartGameButtonPressed()
    {
        Debug.Log("Start Game Button is pressed");
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    private void OnOptionsButtonPressed()
    {
        Debug.Log("Options Button is pressed");
        OptionButtonPressedEvent?.Invoke(this, new OptionButtonPressedEventArgs(_mainMenuContentPanel));
    }

    private void OnMadeByButtonPressed()
    {
        Debug.Log("Made by button is pressed");
        MadeByButtonPresseEvent?.Invoke(this, new MadeByButtonPresseEventArgs(_mainMenuContentPanel));
    }
    
    private void OnExitGameButtonPressed()
    {
        ExitGameButtonEvent?.Invoke(this, new ExitGameButtonEventArgs());
    }

    private void OnBackButtonPressed()
    {
        Debug.Log("Back button is pressed");
        BackbButtonPressedEvent?.Invoke(this, new BackbButtonPressedEventArgs());
    }
}
