﻿using System;
using UnityEngine;

public class OptionButtonPressedEventArgs:EventArgs
{
	public Transform Parent;

	public OptionButtonPressedEventArgs(Transform parent)
	{
		Parent = parent;
	}
}

public class MadeByButtonPresseEventArgs:EventArgs
{
	public Transform Parent;

	public MadeByButtonPresseEventArgs(Transform parent)
	{
		Parent = parent;
	}
}

public class ExitGameButtonEventArgs: EventArgs { }

public class BackbButtonPressedEventArgs : EventArgs { }

public class SwitchStateEventArgs : EventArgs { }

