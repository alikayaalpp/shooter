﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenuState : GameState
{
    #region Fields
    private OptionsMenuStateModel _model;
    private OptionsMenuStateView _view;

    private AudioView _audioView;
    private AudioController _audioController;
    private Audio _audioModel;

    private HowToPlayView _howToPlayView;
    private HowToPlay _howToPlayModel;
    private HowToPlayController _howToPlayController;

    private VideoView _videoView;
    private Video _videoModel;
    private VideoController _videoController;
    #endregion

    public OptionsMenuState(OptionsMenuStateView view, OptionsMenuStateModel model)
    {
        _model = model;
        _view = view;
    }
    public override void Begin()
    {
        base.Begin();
        RegisterEvents();
        InitializeInnerMVCs();

    }

    public override void End()
    {
        base.End();
        UnregisterEvents();
        GameObject.Destroy(_view.gameObject);
    }

    private void RegisterEvents()
    {
        _view.AudioButtonPressedEvent += OnAudioButtonPressed;
        _view.VideoButtonPressedEvent += OnVideoButtonPressed;
        _view.HowToPlayButtonPressedEvent += OnHowToPlayButtonPressed;
    }

    private void UnregisterEvents()
    {
        _view.AudioButtonPressedEvent -= OnAudioButtonPressed;
        _view.VideoButtonPressedEvent -= OnVideoButtonPressed;
        _view.HowToPlayButtonPressedEvent -= OnHowToPlayButtonPressed;
    }

    private void InitializeInnerMVCs()
    {
        Debug.Log("They are initializing");
        _audioView= OptionsFactory.Instance.CreateAudioView(_view.ContentPanel);
        _audioModel = OptionsFactory.Instance.CreateAudio();
        _audioController = OptionsFactory.Instance.CreateAudioController(_audioView, _audioModel);

        _howToPlayView = OptionsFactory.Instance.CreateHowToPlayView(_view.ContentPanel);
        _howToPlayModel = OptionsFactory.Instance.CreateHowToPlay();
        _howToPlayController = OptionsFactory.Instance.CreateHowToPlayController(_howToPlayView, _howToPlayModel);

        _videoView = OptionsFactory.Instance.CreateVideoView(_view.ContentPanel);
        _videoModel = OptionsFactory.Instance.CreateVideo();
        _videoController = OptionsFactory.Instance.CreateVideoController(_videoView, _videoModel);

        _videoView.gameObject.SetActive(false);
        _howToPlayView.gameObject.SetActive(false);
        _audioView.gameObject.SetActive(false);
    }

    public void OnAudioButtonPressed(object sender, AudioButtonPressedEventArgs eventArgs)
    {
        _videoView.gameObject.SetActive(false);
        _howToPlayView.gameObject.SetActive(false);
        _audioView.gameObject.SetActive(true);
    }

    public void OnVideoButtonPressed(object sender, VideoButtonPressedEventArgs eventArgs)
    {
        _videoView.gameObject.SetActive(true);
        _howToPlayView.gameObject.SetActive(false);
        _audioView.gameObject.SetActive(false);
    }

    public void OnHowToPlayButtonPressed(object sender, HowToPlayButtonPressedEventArgs eventArgs)
    {
        _videoView.gameObject.SetActive(false);
        _howToPlayView.gameObject.SetActive(true);
        _audioView.gameObject.SetActive(false);
    }
}