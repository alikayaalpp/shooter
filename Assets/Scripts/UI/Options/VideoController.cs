﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoController
{
    private VideoView _view;
    private Video _model;

    public VideoController(VideoView view, Video model)
    {
        _view = view;
        _model = model;
    }
}
