﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenuStateView : MonoBehaviour
{
    #region Fields
    [SerializeField] private Button _audioButton;
    [SerializeField] private Button _howToPlayButton;
    [SerializeField] private Button _videoButton;
    [SerializeField] private Transform _contentPanel;

    public Transform ContentPanel
    {
        get { return _contentPanel; }
    }
    #endregion

    #region EventHandlers
    public event EventHandler<AudioButtonPressedEventArgs> AudioButtonPressedEvent;
    public event EventHandler<HowToPlayButtonPressedEventArgs> HowToPlayButtonPressedEvent;
    public event EventHandler<VideoButtonPressedEventArgs> VideoButtonPressedEvent;
    #endregion
    void Start()
    {
        _audioButton.onClick.AddListener(OnAudioButtonPressed);
        _howToPlayButton.onClick.AddListener(OnHowToPlayButtonPressed);
        _videoButton.onClick.AddListener(OnVideoButtonPressed);
    }

    private void OnAudioButtonPressed()
    {
        AudioButtonPressedEvent?.Invoke(this, new AudioButtonPressedEventArgs());
    }

    private void OnHowToPlayButtonPressed()
    {
        HowToPlayButtonPressedEvent?.Invoke(this, new HowToPlayButtonPressedEventArgs());
    }

    private void OnVideoButtonPressed()
    {
        VideoButtonPressedEvent?.Invoke(this, new VideoButtonPressedEventArgs());
    }
}
