﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPlayController
{
    private HowToPlayView _view;
    private HowToPlay _model;

    public HowToPlayController(HowToPlayView view, HowToPlay model)
    {
        _view = view;
        _model = model;
    }
}
