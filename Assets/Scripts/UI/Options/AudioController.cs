﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController
{
    // Start is called before the first frame update
    private AudioView _view;
    private Audio _model;

    public AudioController(AudioView view, Audio model)
    {
        _view = view;
        _model = model;
    }

    private void RegisterEvents()
    {
        
    }

    private void UnRegisterEvents()
    {

    }

}
