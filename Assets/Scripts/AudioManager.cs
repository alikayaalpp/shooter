﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager 
{
    #region Fields
    private AudioClip _machineGunFire;
    private AudioClip _enemyDeathSound;
    private AudioSource _gunFireSource;

    public AudioClip MachineGunFire
    { 
        get { return _machineGunFire; }
    }

    public AudioClip EnemyDeathSound
    {
        get { return _enemyDeathSound; }
    }

    #endregion
    public AudioManager()
	{
        _machineGunFire = Resources.Load<AudioClip>("Audios/MachineGunFireSound");
        _enemyDeathSound = Resources.Load<AudioClip>("Audios/EnemyDeathSound");
	}

    public void PlayBackgroundMusic(AudioClip clip, AudioSource source)
    {
        source.clip = clip;
        source.loop = true;
        source.Play();
    }

    public void PlayMonsterDeadSound(AudioClip clip, AudioSource source)
    {
        source.clip = clip;
        source.Play();
    }

    public void PlayGunFireMusic(AudioClip clip, AudioSource source)
    {
        source.clip = clip;
        source.Play();
    }
}
