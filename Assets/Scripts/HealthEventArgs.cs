﻿using UnityEngine;
using System;

public class TakeDamageEventArgs: EventArgs
{
	public int Amount;
	public TakeDamageEventArgs(int amount)
	{
		Amount = amount;
	}
}
