﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState
{
    public virtual void Begin()
    {

    }

    public virtual void End()
    {

    }
}
