﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerMoveForwardEventArgs:EventArgs
{
    public GameObject Player;

	public PlayerMoveForwardEventArgs(GameObject player)
	{
		Player = player;
	}
}

public class PlayerMoveBackWardEventArgs:EventArgs
{
	public GameObject Player;

	public PlayerMoveBackWardEventArgs(GameObject player)
	{
		Player = player;
	}
}

public class PlayerMoveRightEventArgs:EventArgs
{
	public GameObject Player;

	public PlayerMoveRightEventArgs(GameObject player)
	{
		Player = player;
	}
}

public class PlayerMoveLeftEventArgs:EventArgs
{
	public GameObject Player;

	public PlayerMoveLeftEventArgs(GameObject player)
	{
		Player = player;
	}
}

public class PlayerRotateAroundEventArgs:EventArgs
{
	public GameObject Player;
	public float RotationY;

	public PlayerRotateAroundEventArgs(GameObject player, float rotationY)
	{
		Player = player;
		RotationY = rotationY;
	}
}

