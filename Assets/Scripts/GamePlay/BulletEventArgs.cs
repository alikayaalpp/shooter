﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



public class DestroyMachineGunNormalBulletEventArgs:EventArgs
{
    public GameObject Bullet;

	public DestroyMachineGunNormalBulletEventArgs(GameObject bullet)
	{
		Bullet = bullet;
	}
}
