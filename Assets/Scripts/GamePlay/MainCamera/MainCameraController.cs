﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraController
{
    #region Fields
    private MainCameraView _view;
    private MainCamera _model;
    #endregion

    public MainCameraController(MainCameraView view, MainCamera model)
    {
        _view = view;
        _model = model;
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        _view.MainCameraSetPositionEvent += _model.OnSetMainCameraPosition;
    }

    public void UnregisterEvents()
    {
        _view.MainCameraSetPositionEvent -= _model.OnSetMainCameraPosition;
    }
}
