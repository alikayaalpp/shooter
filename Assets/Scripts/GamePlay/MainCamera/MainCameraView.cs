﻿using UnityEngine;
using System;

public class MainCameraView : MonoBehaviour
{
    #region Fields
    private Transform _player;

    public Transform Player
    {
        set { _player = value; }
    }
    #endregion

    #region EventHandlers
    public event EventHandler<MainCameraSetPositionEventArgs> MainCameraSetPositionEvent;
    #endregion

    void LateUpdate()
    {
        //transform.position = new Vector3(_player.position.x, transform.position.y, _player.position.z);
        MainCameraSetPositionEvent?.Invoke(this, new MainCameraSetPositionEventArgs(this.gameObject, _player.gameObject));
    }
}
