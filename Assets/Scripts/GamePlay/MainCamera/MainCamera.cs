﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera
{
    #region Fields
    private float _yOffsetValue = 22.09f;
    #endregion
    public void OnSetMainCameraPosition(object sender, MainCameraSetPositionEventArgs eventArgs)
    {
        eventArgs.Camera.transform.position = new Vector3(eventArgs.Player.transform.position.x, eventArgs.Camera.transform.position.y, eventArgs.Player.transform.position.z);
    }
}
