﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Health
{
    #region Fields
    private int _currentHealth;
    
    protected int CurrentHealth
    {
        get { return _currentHealth; }
        set { _currentHealth = value; }
    }

    public abstract void TakeDamage(object sender , TakeDamageEventArgs eventArgs);

    protected abstract void InstantHeal();

    protected abstract void HealInTime();
    #endregion
}

interface IUpgradeHealth
{
    void UpgradeHealth();
}
