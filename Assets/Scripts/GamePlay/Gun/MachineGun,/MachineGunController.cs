﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGunController
{
    #region Fields
    private MachineGunView _view;
    private MachineGun _model;
    #endregion

    public MachineGunController(MachineGunView view, MachineGun model)
    {
        _view = view;
        _model = model;
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        _view.NormalFireEvent += _model.OnNormalFire;
    }

    private void UnregisterEvents()
    {
        _view.NormalFireEvent -= _model.OnNormalFire;
    }
}
