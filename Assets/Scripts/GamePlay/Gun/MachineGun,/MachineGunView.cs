﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGunView : GunView
{
    #region Fields
    private GameObject _bullet;
    private float _normalFireTimer = 0;
    private float _chargeFireTimer = 0;
    private float _normalFireFrequancy = 0.1f;
    private float _chargeFireFrequancy = 1;
    [SerializeField] private Transform _spawnPoint;
    #endregion

    #region EventArgs
    public event EventHandler<NormalFireEventArgs> NormalFireEvent;
    public event EventHandler<ChargeFireEventArgs> ChargeFireEvent;
    #endregion
    

    void Update()
    {
        _normalFireTimer += Time.deltaTime;
        _chargeFireFrequancy += Time.deltaTime;
        if(Input.GetButton("Fire1") && _normalFireTimer>_normalFireFrequancy)
        {
            _normalFireTimer = 0;
            NormalFireEvent?.Invoke(this, new NormalFireEventArgs(_spawnPoint));
        }

        if(Input.GetButton("Fire2") && _chargeFireTimer > _chargeFireFrequancy)
        {
            _chargeFireTimer = 0;
            ChargeFireEvent?.Invoke(this, new ChargeFireEventArgs());
        }
    }
}
