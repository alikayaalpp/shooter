﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGun
{
    public void OnNormalFire(object sender, NormalFireEventArgs eventArgs)
    {
        MachineGunBulletView view = BulletFactory.Instance.CreateMachineGunBulletView(eventArgs.SpawnPoint);
        MachineGunBullet model = BulletFactory.Instance.CreateMachineGunBullet();
        MachineGunBulletController controller = BulletFactory.Instance.CreateMachinbeGunBulletController(view, model);
        GameController.Instance.AudioManager.PlayGunFireMusic(GameController.Instance.AudioManager.MachineGunFire, GameController.Instance.GunFireSource);
        GameObject.Destroy(view.gameObject, 10);
    }

    public void OnChargeFire(object sender, ChargeFireEventArgs eventArgs)
    {

    }
}
