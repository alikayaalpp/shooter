﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MachineGunBulletView : MonoBehaviour {
    #region Fields
    private Rigidbody _rigidbody;
    private float speed = 15;
    #endregion

    #region EventHandlers
    public event EventHandler<DestroyMachineGunNormalBulletEventArgs> DestroyMachineGunNormalBulletEvent;
    #endregion
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        this.gameObject.transform.SetParent(null);
        _rigidbody.velocity -= transform.right*speed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag!="Bullet")
        {
            DestroyMachineGunNormalBulletEvent?.Invoke(this, new DestroyMachineGunNormalBulletEventArgs(this.gameObject));
        }
    }
}
