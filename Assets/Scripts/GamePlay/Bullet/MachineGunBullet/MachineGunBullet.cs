﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGunBullet
{
    public void OnDestroyMachineGunBullet(object sender, DestroyMachineGunNormalBulletEventArgs eventArgs)
    {
        GameObject.Destroy(eventArgs.Bullet);
    }
}
