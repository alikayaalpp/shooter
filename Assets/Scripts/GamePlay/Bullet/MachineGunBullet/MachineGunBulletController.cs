﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGunBulletController 
{
    #region Fields
    private MachineGunBulletView _view;
    private MachineGunBullet _model;
    #endregion
    public MachineGunBulletController(MachineGunBulletView view, MachineGunBullet model)
    {
        _view = view;
        _model = model;
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        _view.DestroyMachineGunNormalBulletEvent += _model.OnDestroyMachineGunBullet;
    }

    private void UnregisterEvents()
    {

    }
}
