﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement
{
    #region Fields
    private float _movementSpeed = 0.15f;
    private float _rotationSpeed = 5f;
    #endregion
    public void OnPlayerMoveRight(object sender, PlayerMoveRightEventArgs eventArgs)
    {
        eventArgs.Player.transform.position += eventArgs.Player.transform.right*_movementSpeed;
    }

    public void OnPlayerMoveLeft(object sender, PlayerMoveLeftEventArgs eventArgs)
    {
        eventArgs.Player.transform.localPosition -= eventArgs.Player.transform.right * _movementSpeed;
    }

    public void OnPlayerMoveForward(object sender, PlayerMoveForwardEventArgs eventArgs)
    {
        eventArgs.Player.transform.localPosition += eventArgs.Player.transform.forward* _movementSpeed;
    }

    public void OnPlayerMoveBackward(object sender, PlayerMoveBackWardEventArgs eventArgs)
    {
        eventArgs.Player.transform.localPosition -= eventArgs.Player.transform.forward * _movementSpeed;
    }

    public void OnPlayerRotateAround(object sender, PlayerRotateAroundEventArgs eventArgs)
    {
        eventArgs.Player.transform.localEulerAngles += new Vector3(eventArgs.Player.transform.rotation.x, eventArgs.RotationY* _rotationSpeed, eventArgs.Player.transform.rotation.z);
    }

    public void SetRotationSpeed()
    {
        //Mouse sensivity
    }
}
