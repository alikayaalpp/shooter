﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthController
{
    #region
    private PlayerHealthView _view;
    private PlayerHealth _model;

    public PlayerHealthController(PlayerHealthView view, PlayerHealth model)
    {
        _view = view;
        _model = model;
    }
    #endregion
}
