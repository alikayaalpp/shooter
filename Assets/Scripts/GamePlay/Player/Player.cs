﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player 
{
    #region Fields
    private float _experiance = 0;
    private int _playerLevel = 1;
    #endregion

    public void OnSetExperiance()
    {
        //Set current xp from local files by usinf PlayerPrefs.
    }

    public void OnSetPlayerLevel()
    {
        if(PlayerPrefs.GetInt("PlayerLevel")<1)
        {
            return;
        }
        else
        {
            _playerLevel = PlayerPrefs.GetInt("PlayerLevel");
        }
    }

    public void OnLevelUp()
    {
        _playerLevel++;
        PlayerPrefs.SetFloat("PlayerLevel", _playerLevel);
    }
}
