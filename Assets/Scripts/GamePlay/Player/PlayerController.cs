﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController
{
    #region Fields
    private PlayerView _view;
    private Player _model;

    private PlayerHealthView _playerHealthView;
    private PlayerHealth _playerHealthModel;
    private PlayerHealthController _playerHealthController;

    private PlayerMovementView _playerMovementView;
    private PlayerMovement _playerMovementModel;
    private PlayerMovementController _playerMovementController;

    private GunView _gunView;
    #endregion
    public PlayerController(PlayerView view, Player model)
    {
        _view = view;
        _model = model;
        RegisterEvents();
        InitializeInnerMVCs();
    }

    private void InitializeInnerMVCs()
    {
        _view.gameObject.GetComponent<PlayerHealthView>().enabled = true;
        _playerHealthView = _view.gameObject.GetComponent<PlayerHealthView>();
        _playerHealthModel = PlayerFactory.Instance.CreatePlayerHealth();
        _playerHealthController = PlayerFactory.Instance.CreatePlayerHealthController(_playerHealthView, _playerHealthModel);

        _view.gameObject.GetComponent<PlayerMovementView>().enabled = true;
        _playerMovementView = _view.gameObject.GetComponent<PlayerMovementView>();
        _playerMovementModel = PlayerFactory.Instance.CreatePlayerMovement();
        _playerMovementController = PlayerFactory.Instance.CreatePlayerMovementController(_playerMovementView, _playerMovementModel);

        _gunView = GunFactory.Instance.CreateMachineGunView(_view.WeaponContainer);
        MachineGun gunModel = GunFactory.Instance.CreateMachineGun();
        MachineGunController gunController = GunFactory.Instance.CreateMachineGunController((_gunView as MachineGunView), gunModel);
    }

    private void RegisterEvents()
    {

    }

    private void UnregisterEvents()
    {

    }
}
