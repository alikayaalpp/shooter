﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementView : MonoBehaviour
{
    #region Fields
    private Rigidbody _rigidbody;
    private Animator _anim;
    private float _horizontalAxis = 0;
    private float _verticalAxis = 0;
    [SerializeField] private int _xClamp = 53;
    [SerializeField] private int _zClamp = 53;
    #endregion

    #region EventHandlers
    public event EventHandler<PlayerMoveForwardEventArgs> PlayerMoveForwardEvent;
    public event EventHandler<PlayerMoveBackWardEventArgs> PlayerMoveBackWardEvent;
    public event EventHandler<PlayerMoveLeftEventArgs> PlayerMoveLeftEvent;
    public event EventHandler<PlayerMoveRightEventArgs> PlayerMoveRightEvent;
    public event EventHandler<PlayerRotateAroundEventArgs> PlayerRotateAroundEvent;
    #endregion
    void Start()
    {
        _rigidbody = this.gameObject.GetComponent<Rigidbody>();
        _anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        Move();
        RotateAround();
        //_rigidbody.MovePosition(new Vector3(Mathf.Clamp(_rigidbody.position.x, -_xClamp, _xClamp), transform.position.y, Mathf.Clamp(_rigidbody.position.z, -_zClamp, _zClamp)));
        //transform.position=new Vector3(Mathf.Clamp(transform.position.x, -_xClamp, _xClamp), transform.position.y, Mathf.Clamp(transform.position.z, -_zClamp, _zClamp));
        _horizontalAxis = Input.GetAxisRaw("Horizontal");
        _verticalAxis = Input.GetAxisRaw("Vertical");
        _anim.SetFloat("horizontal", _horizontalAxis);
        _anim.SetFloat("vertical", _verticalAxis);
    }

    private void Move()
    {
        if (Input.GetKey(KeyCode.W))
        {
            PlayerMoveForwardEvent?.Invoke(this, new PlayerMoveForwardEventArgs(this.gameObject));
        }

        if (Input.GetKey(KeyCode.S))
        {
            PlayerMoveBackWardEvent?.Invoke(this, new PlayerMoveBackWardEventArgs(this.gameObject));
        }

        if(Input.GetKey(KeyCode.A))
        {
            PlayerMoveLeftEvent?.Invoke(this, new PlayerMoveLeftEventArgs(this.gameObject));
        }

        if(Input.GetKey(KeyCode.D))
        {
            PlayerMoveRightEvent?.Invoke(this, new PlayerMoveRightEventArgs(this.gameObject));
        }
    }

    private void RotateAround()
    {
        PlayerRotateAroundEvent?.Invoke(this, new PlayerRotateAroundEventArgs(this.gameObject, Input.GetAxis("Mouse X")));
    }
}
