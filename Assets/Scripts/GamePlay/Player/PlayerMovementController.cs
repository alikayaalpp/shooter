﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController
{
    #region Fields
    private PlayerMovementView _view;
    private PlayerMovement _model;
    #endregion
    public PlayerMovementController(PlayerMovementView view, PlayerMovement model)
    {
        _view = view;
        _model = model;
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        _view.PlayerMoveForwardEvent += _model.OnPlayerMoveForward;
        _view.PlayerMoveBackWardEvent += _model.OnPlayerMoveBackward;
        _view.PlayerMoveRightEvent += _model.OnPlayerMoveRight;
        _view.PlayerMoveLeftEvent += _model.OnPlayerMoveLeft;
        _view.PlayerRotateAroundEvent += _model.OnPlayerRotateAround;
    }

    private void UnregisterEvents()
    {
        _view.PlayerMoveForwardEvent -= _model.OnPlayerMoveForward;
        _view.PlayerMoveBackWardEvent -= _model.OnPlayerMoveBackward;
        _view.PlayerMoveRightEvent -= _model.OnPlayerMoveRight;
        _view.PlayerMoveLeftEvent -= _model.OnPlayerMoveLeft;
        _view.PlayerRotateAroundEvent -= _model.OnPlayerRotateAround;
    }
 
}
