﻿using System;
using UnityEngine;

public class MainCameraSetPositionEventArgs:EventArgs
{
    public GameObject Camera;
	public GameObject Player;

	public MainCameraSetPositionEventArgs(GameObject camera,GameObject player)
	{
		Camera = camera;
		Player = player;
	}
}
