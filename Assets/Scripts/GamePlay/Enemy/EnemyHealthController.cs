﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthController
{
    #region Fields
    private EnemyHealthView _view;
    private EnemyHealth _model;
    #endregion

    #region EventHandlers
    public event EventHandler<EnemyDieEventArgs> EnemyDieEvent;
    #endregion
    public EnemyHealthController(EnemyHealthView view, EnemyHealth model)
    {
        _view = view;
        _model = model;
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        _view.TakeDamageEvent += _model.TakeDamage;
        _model.EnemyDieEvent += OnEnemyDie;
    }

    public void UnregisterEvents()
    {
        _view.TakeDamageEvent -= _model.TakeDamage;
        _model.EnemyDieEvent -= OnEnemyDie;
    }

    public void OnEnemyDie(object sender, EnemyDieEventArgs eventArgs)
    {
        EnemyDieEvent?.Invoke(this, new EnemyDieEventArgs());
    }
}
