﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovementView : MonoBehaviour
{
    #region Fields
    private NavMeshAgent _agent;
    private GameObject _player;
    #endregion

    #region EventHandlers
    public event EventHandler<DestroyEnemyEventArgs> DestroyEnemyEvent;
    #endregion
    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
    }
 
    public void SetPalyerObject(GameObject Player)
    {
        _player = Player;
    }

    private void FixedUpdate()
    {
        if(_player!=null && _agent!=null && _agent.isActiveAndEnabled)
        {
            _agent.SetDestination(_player.transform.position);
        }
    }

    public void OnEnemyDie(object sender, EnemyDieEventArgs eventArgs)
    {
        CloseAnimatorAndAgent();
        DestroyEnemyEvent?.Invoke(this, new DestroyEnemyEventArgs());
    }
    private void CloseAnimatorAndAgent()
    {
        _agent.enabled = false;
        GetComponent<Animator>().enabled = false;
    }
}
