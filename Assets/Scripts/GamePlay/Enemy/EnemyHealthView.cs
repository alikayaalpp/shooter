﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyHealthView : MonoBehaviour
{
    #region Fields
    public event EventHandler<TakeDamageEventArgs> TakeDamageEvent;
    #endregion
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="Bullet")
        {
            TakeDamageEvent?.Invoke(this, new TakeDamageEventArgs(20));
            GetComponentInChildren<ParticleSystem>().Play();
        }
    }
}
