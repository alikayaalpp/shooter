﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyMovementController 
{
    #region Fields
    private EnemyMovementView _view;
    private EnemyMovement _model;
    #endregion

    #region EventHandler
    public event EventHandler<DestroyEnemyEventArgs> DestroyEnemyEvent;
    #endregion
    public EnemyMovementController(EnemyMovementView view, EnemyMovement model)
    {
        _view = view;
        _model = model;
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        _view.DestroyEnemyEvent += OnDestroyEnemy;
    }

    public void UnregisterEvents()
    {
        _view.DestroyEnemyEvent -= OnDestroyEnemy;
    }

    private void OnDestroyEnemy(object sender, DestroyEnemyEventArgs eventArgs)
    {
        DestroyEnemyEvent?.Invoke(this, new DestroyEnemyEventArgs());

    }
}
