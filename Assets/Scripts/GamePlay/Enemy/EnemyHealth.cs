﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : Health
{
    #region Fields
    public event EventHandler<EnemyDieEventArgs> EnemyDieEvent;
    #endregion
    private int _health = 100;
    private bool _isDead = false;
    protected override void HealInTime()
    {
        throw new System.NotImplementedException();
    }

    protected override void InstantHeal()
    {
        throw new System.NotImplementedException();
    }


    public override void TakeDamage(object sender, TakeDamageEventArgs eventArgs)
    {
        _health -= eventArgs.Amount;

        if(_health<0 && !_isDead)
        {
            _isDead = true;
            GameController.Instance.TotalMonster--;
            EnemyDieEvent?.Invoke(this, new EnemyDieEventArgs());
            GameController.Instance.AudioManager.PlayMonsterDeadSound(GameController.Instance.AudioManager.EnemyDeathSound, GameController.Instance.MonsterDeathSource);
        }
    }
}
