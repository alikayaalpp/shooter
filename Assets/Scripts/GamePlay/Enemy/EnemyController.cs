﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyController
{
    #region Fields
    private EnemyView _view;
    private Enemy _model;

    private EnemyHealthController _enemyHealthController;
    private EnemyMovementView _enemyMovementView;
    private EnemyMovementController _enemyMovementController;
    #endregion

    #region EventHandlers
    public event EventHandler<EnemyDieEventArgs> EnemyDieEvent;
    #endregion

    public EnemyController(EnemyView view, Enemy model)
    {
        _view = view;
        _model = model;
        InitializeInnerMVCs();
        RegisterEvents();
    }

    private void RegisterEvents()
    {
        _enemyHealthController.EnemyDieEvent += OnEnemyDie;
        EnemyDieEvent += _enemyMovementView.OnEnemyDie;
        _enemyMovementController.DestroyEnemyEvent += OnDestroyEnemy;
    }

    private void UnregisterEvents()
    {
        _enemyMovementController.DestroyEnemyEvent -= OnDestroyEnemy;
        _enemyHealthController.EnemyDieEvent -= OnEnemyDie;
        EnemyDieEvent += _enemyMovementView.OnEnemyDie;
        _enemyHealthController.UnregisterEvents();
        _enemyMovementController.UnregisterEvents();
        GameObject.Destroy(_view.gameObject,2);
    }
    
    private void InitializeInnerMVCs()
    {
        EnemyHealthView enemyHealthView = _view.GetComponent<EnemyHealthView>();
        enemyHealthView.enabled = true;
        EnemyHealth enemyHealthModel = EnemyFactory.Instance.CreateEnemyHealth();
        _enemyHealthController = EnemyFactory.Instance.CreateEnemyHealthController(enemyHealthView, enemyHealthModel);

        _enemyMovementView =_view.GetComponent<EnemyMovementView>();
        EnemyMovement enemyMovementModel = EnemyFactory.Instance.CreateEnemyMovement();
        _enemyMovementController = EnemyFactory.Instance.CreateEnemyMovementController(_enemyMovementView, enemyMovementModel);
        _enemyMovementView.enabled = true;
        _enemyMovementView.SetPalyerObject(_view.Player);
    }

    private void OnEnemyDie(object sender, EnemyDieEventArgs eventArgs)
    {
        EnemyDieEvent?.Invoke(this, new EnemyDieEventArgs());
    }

    private void OnDestroyEnemy(object sender, DestroyEnemyEventArgs eventArgs)
    {
        UnregisterEvents();
    }

}
